// Copyright 2018-Future norlin (myxaxaxa@gmail.com). All Rights Reserved.

#pragma once

#include "TextFilter.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TFTypes.h"
#include "TFFilter.generated.h"

/**
 * Base Filter class to define the overall text transformation logic
 */
UCLASS(Blueprintable, BlueprintType)
class TEXTFILTER_API UTFFilter : public UObject {
	GENERATED_BODY()

public:
	UTFFilter();

	// The entry point for the text transformation. Should apply all the transforms here and return the resulting string
	UFUNCTION(BlueprintNativeEvent, Category = "Text Filter")
	FString Process(const FString& InText);

	// Apply an existing text filter to the text entry
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter")
	static FString ApplyTextFilter(TSubclassOf<UTFFilter> FilterClass, const FString& InText);

	// Limit the text to specific count of characters.
	// If CountAlphanumericOnly == true then all non-alphanumeric characters are not counted towards the MaxCharacters limit
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformLimitCharacters(const FString& InText, int32 MaxCharacters, bool bCountAlphanumericOnly = false, float Probability = 1.0f, bool bEnabled = true);

	// Limit the text to specific count of words (separated by whitespace or comma)
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformLimitWords(const FString& InText, int32 MaxWords, float Probability = 1.0f, bool bEnabled = true);

	// Remove all non-alphanumeric characters (including whitespaces)
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformAllowAlnumOnly(const FString& InText, float Probability = 1.0f, bool bEnabled = true);

	// Remove all whitespaces
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformRemoveWhitespaces(const FString& InText, float Probability = 1.0f, bool bEnabled = true);

	// Adds one of the patterns (randomly selected) before or after the text
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformAddPattern(const FString& InText, TArray<FString> Patterns, ETFPatternPosition Position = ETFPatternPosition::Random, float Probability = 1.0f, bool bEnabled = true);

	// Adds one of the patterns (randomly selected) before or after each of the words
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformAddPatternForWords(const FString& InText, TArray<FString> Patterns, bool bReplaceWhitespaces = false, float EachProbability = 1.0f, float Probability = 1.0f, bool bEnabled = true);

	// Capitalize each word (separated by whitespace), all other to lower case
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformCapitalize(const FString& InText, float Probability = 1.0f, bool bEnabled = true);

	// Capitalize every odd character
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Text Filter", meta = (AdvancedDisplay = "Probability,bEnabled"))
	static FString TransformCapitalizeOthers(const FString& InText, bool bApplyToOdds, float Probability = 1.0f, bool bEnabled = true);
};
