// Copyright 2018-Future norlin (myxaxaxa@gmail.com). All Rights Reserved.

#pragma once

UENUM(BlueprintType)
enum class ETFPatternPosition : uint8 {
	Random UMETA(Tooltip = "Randomly decide if to place the pattern before or after the text (but not both)"),
	Before UMETA(Tooltip = "Place the pattern before the text"),
	After UMETA(Tooltip = "Place the pattern after the text"),
	Both UMETA(Tooltip = "Place the pattern both before and after the text")
};

UENUM(BlueprintType)
enum class ETFEntityType : uint8 {
	Sentence UMETA(Tooltip = "Apply to the whole sentence"),
	Word UMETA(Tooltip = "Apply to each word"),
	Character UMETA(Tooltip = "Apply to each character")
};
