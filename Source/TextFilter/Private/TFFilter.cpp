// Copyright 2018-Future norlin (myxaxaxa@gmail.com). All Rights Reserved.

#include "TFFilter.h"

UTFFilter::UTFFilter() {
}

bool ShouldSkip(const FString& InText, float Probability, bool bEnabled) {
	return InText.IsEmpty() || !bEnabled || FMath::FRand() > Probability;
}

FString CharToString(const TCHAR& InChar) {
	FString Result;
	Result.AppendChar(InChar);
	return Result;
}

FString ToLowerInternational(const FString& InText) {
	// Use FText since it's the only way to handle international text
	FText Input = FText::FromString(InText);
	Input = Input.ToLower();

	return Input.ToString();
}

FString ToUpperInternational(const FString& InText) {
	// Use FText since it's the only way to handle international text
	FText Input = FText::FromString(InText);
	Input = Input.ToUpper();

	return Input.ToString();
}

TArray<FString> ParseIntoWords(const FString& InText) {
	TArray<TCHAR> Chars = InText.GetCharArray();
	TArray<FString> Result;

	FString CurrentToken;
	for (auto Char : Chars) {
		if (FChar::IsAlnum(Char)) {
			CurrentToken.AppendChar(Char);
		} else {
			if (CurrentToken.Len() > 0) {
				Result.Add(CurrentToken);
				CurrentToken = FString();
			}

			Result.Add(CharToString(Char));
		}
	}

	if (CurrentToken.Len() > 0) {
		Result.Add(CurrentToken);
	}

	return Result;
}

FString UTFFilter::Process_Implementation(const FString& InText) {
	// Do nothing, the transformation logic should be defined in a derived class
	return FString(InText);
}

FString UTFFilter::ApplyTextFilter(TSubclassOf<UTFFilter> FilterClass, const FString& InText) {
	UTFFilter* Filter = FilterClass.GetDefaultObject();
	if (!ensure(Filter)) {
		// If no valid Filter, return unmodified text
		return FString(InText);
	}

	// Apply the Filter's rules
	return Filter->Process(InText);
}

FString UTFFilter::TransformLimitCharacters(const FString& InText, int32 MaxCharacters, bool bCountAlphanumericOnly, float Probability, bool bEnabled) {
	if (ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	TArray<TCHAR> Chars = InText.GetCharArray();

	FString Result = FString(TEXT(""));
	int32 Length = 0;
	for (int32 i = 0; i < InText.Len(); i += 1) {
		auto Char = InText[i];
		Result = Result.AppendChar(Char);

		if (FChar::IsAlnum(Char) || !bCountAlphanumericOnly) {
			Length += 1;
		}

		if (Length == MaxCharacters) {
			break;
		}
	}

	return Result;
}

FString UTFFilter::TransformLimitWords(const FString& InText, int32 MaxWords, float Probability, bool bEnabled) {
	if (ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	TArray<FString> Words;
	InText.ParseIntoArrayWS(Words, TEXT(","), true);
	if (Words.Num() > MaxWords) {
		Words.SetNum(MaxWords, true);
	}

	FString Result = FString::Join(Words, TEXT(" "));

	return Result;
}

FString UTFFilter::TransformAllowAlnumOnly(const FString& InText, float Probability, bool bEnabled) {
	if (ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	TArray<TCHAR> Chars = InText.GetCharArray();

	FString Result = FString(TEXT(""));

	for (int32 i = 0; i < InText.Len(); i += 1) {
		auto Char = InText[i];

		if (!FChar::IsAlnum(Char)) {
			continue;
		}

		Result = Result.AppendChar(Char);
	}

	return Result;
}

FString UTFFilter::TransformRemoveWhitespaces(const FString& InText, float Probability, bool bEnabled) {
	if (ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	TArray<TCHAR> Chars = InText.GetCharArray();

	FString Result = FString(TEXT(""));

	for (int32 i = 0; i < InText.Len(); i += 1) {
		auto Char = InText[i];

		if (FChar::IsWhitespace(Char)) {
			continue;
		}

		Result = Result.AppendChar(Char);
	}

	return Result;
}

FString UTFFilter::TransformAddPattern(const FString& InText, TArray<FString> Patterns, ETFPatternPosition Position, float Probability, bool bEnabled) {
	if (Patterns.Num() == 0 || ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	int32 PatternIndex = FMath::RandRange(0, Patterns.Num() - 1);
	FString Pattern = Patterns[PatternIndex];

	FString Result = InText;

	bool bRandomBefore = FMath::RandBool();
	bool bNeedBefore = Position == ETFPatternPosition::Before || Position == ETFPatternPosition::Both || (Position == ETFPatternPosition::Random && bRandomBefore);
	bool bNeedAfter = Position == ETFPatternPosition::After || Position == ETFPatternPosition::Both || (Position == ETFPatternPosition::Random && !bRandomBefore);

	if (bNeedBefore) {
		Result = Pattern + TEXT(" ") + Result;
	}

	if (bNeedAfter) {
		Result += TEXT(" ") + Pattern;
	}

	return Result;
}

FString UTFFilter::TransformAddPatternForWords(const FString& InText, TArray<FString> Patterns, bool bReplaceWhitespaces, float EachProbability, float Probability, bool bEnabled) {
	if (Patterns.Num() == 0 || ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	TArray<FString> Words = ParseIntoWords(InText);

	FString Result;
	for (FString Word : Words) {
		if (Word.Len() == 0) {
			// TODO: fix this, should not happens
			UE_LOG(LogTemp, Warning, TEXT("WTF how we got a null-length word here?"));
			continue;
		}

		auto FirstChar = Word[0];
		if (FChar::IsWhitespace(FirstChar) && bReplaceWhitespaces) {
			continue;
		}

		Result += Word;

		if (!FChar::IsAlnum(FirstChar)) {
			// Don't add the pattern to punctuation
			continue;
		}

		if (FMath::FRand() > EachProbability) {
			continue;
		}

		int32 PatternIndex = FMath::RandRange(0, Patterns.Num() - 1);
		FString Pattern = Patterns[PatternIndex];

		Result += Pattern;
	}


	return Result;
}

FString UTFFilter::TransformCapitalize(const FString& InText, float Probability, bool bEnabled) {
	if (ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	FString Result = ToLowerInternational(InText);

	TArray<FString> Words;
	Result.ParseIntoArrayWS(Words, NULL, true);

	TArray<FString> CapitalizedWords;
	for (FString Word : Words) {
		FString Capitalized = Word;
		FString FirstLetter = CharToString(Word[0]);
		Capitalized[0] = ToUpperInternational(FirstLetter)[0];
		CapitalizedWords.Add(Capitalized);
	}

	Result = FString::Join(CapitalizedWords, TEXT(" "));

	return Result;
}

FString UTFFilter::TransformCapitalizeOthers(const FString& InText, bool bApplyToOdds, float Probability, bool bEnabled) {
	if (ShouldSkip(InText, Probability, bEnabled)) {
		return FString(InText);
	}

	FString Result = ToLowerInternational(InText);

	TArray<TCHAR> Chars = Result.GetCharArray();

	Result = FString(TEXT(""));

	int32 Check = bApplyToOdds ? 0 : 1;

	for (int32 i = 0; i < InText.Len(); i += 1) {
		TCHAR Char;

		if (i % 2 == Check) {
			FString CharString = CharToString(InText[i]);
			Char = ToUpperInternational(CharString)[0];
		} else {
			Char = InText[i];
		}

		Result = Result.AppendChar(Char);
	}

	return Result;
}
